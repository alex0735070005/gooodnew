import React from 'react';

const Product = ({data, addProduct}) => {
    const {
        name,
        manufacturer,
        price,
        image,
        country,
    } = data;

    return (
        <div className='product'>
            <div className='wrap'>
                <h4>{name}</h4>
                <img src={image} alt={name} />
                <p>Made: {manufacturer}</p>
                <p>Country: {country}</p>
                <div>
                    <span>{price}$</span>
                    <input
                        value='Add cart'
                        type='button'
                        className='btn-add-product'
                        onClick={()=>addProduct(data)}
                    />
                </div>
            </div>
        </div>
    )

}

export default Product;