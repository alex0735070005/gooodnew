import React from 'react';

const OrderForm = ({queryOrderHandler, isErrorPhone, isSuccessOrder, setPhone}) => {
    return (
        <div id="order-form-cart">
            <input
                onChange={e => setPhone(e.target.value)}
                type="text"
                placeholder="Entered your phone or email"
            />
            <input                 
                type="button" 
                value="Send order" 
                onClick={queryOrderHandler}
            />
            <div className="message">
                {isErrorPhone ? 'Entered phone or email' : ''}
                {isSuccessOrder ? 'Your data order success' : ''}
            </div>
        </div>
    );
}

export default OrderForm