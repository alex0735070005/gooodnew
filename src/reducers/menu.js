import data from '../data/menu.json'

const initialState = data;

const menu = (state = initialState, action) => {
    
    if (action.type === 'show') {
        console.log('MENU', action);
    }
    return state;
}

export default menu;